/*
* @param {string} calendarCell スケジュールのdiv要素
* @return {object} 日付とイベントのオブジェクト
*   info = {
*     "date": Date,
*     "content": [
*       {"title": "title1", color: EventColor},
*       {"title": "title2", color: EventColor},
*       ...
*     ]
*   }
*/
function extractEventInfo(calendarCell){
  // calendarCellのHTML構造は以下のような感じ。
  // <div class="calendarCell">
  //   <div class="calendarCellDate">dd<span>weekday</span></div>
  //   <div class="calendarCellContent">
  //     <a class="... color ...">title1</a>
  //     <a class="... color ...">title2</a>
  //     ...
  //   </div>
  // </div>
  let calendarCellDate = parser.getElementsByClassName(calendarCell, "calendarCellDate");
  let date = calendarCellDate[0].getText();

  let contents = [];
  let calendarCellContent = parser.getElementsByClassName(calendarCell, "calendarCellContent");
  if (calendarCellContent) {
    // タイトルたちを取得
    contents = parser.getElementsByTagName(calendarCellContent[0], "a")
      .map(ele => {
        // リンクの取得
        let link = ele.getAttribute("href").getValue();
        // タイトルとリンクの組
        return {
          "date": date,
          "title": ele.getValue(),
          "link": link
        }
      })
  }

  return {
    "date": date,
    "contents": contents
  };
}

function formatHtml(res){
  // body 抽出
  res = res.match(/<body(.|\r|\n)*<\/body>/)[0];
  // imgタグ削除。。。
  res = res.replace(/<img .*?>/g, "");
  // scriptタグ削除。。。
  res = res.replace(/<script.*>.*<\/script>/g, "");
  // brタグ削除。。。
  res = res.replace(/<br.*?>/g, "");

  return res;
}

/**
* @param {Date} dt 予定を取得する日付
* @return {Array.<object>} イベント情報リスト
*/
function scraping(dt=new Date()){
  // バンドリ公式サイトからSchedule情報を取得する

  // 年月を取得(getMonth()は 0 baseなので+1)
  const yyyyMM = dt.getFullYear() + ("0"+(dt.getMonth()+1)).slice(-2);

  // htmlを取得後整形
  const url = `https://bang-dream.com/news/schedule?ym=${yyyyMM}`;
  let res = UrlFetchApp.fetch(url).getContentText();
  res = formatHtml(res);

  let doc = XmlService.parse(res);
  let html = doc.getRootElement();
  // 週毎のリスト
  let calendarRows = parser.getElementsByClassName(html, "calendarRow");

  // イベント情報リストを抽出する
  let info = null;
  calendarRows.forEach(row => {
    // 1週ごとに日付セルの中のイベント情報を見ていく
    parser.getElementsByClassName(row, "calendarCell").forEach(calendarCell => {
      let cls = calendarCell.getAttribute("class").getValue();
      if (cls.indexOf("notCurrent") < 0){
        // .notCurrent はその月ではないセルを表す
        tmp = extractEventInfo(calendarCell);
        if (dt.getDate() == tmp.date){
          info = tmp.contents;
        }
      }
    })
  });
  return info;
}
