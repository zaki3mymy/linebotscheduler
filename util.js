/*
 * リストを指定した要素数のリストに分割する
 * ex) arr=[1,2,3,4,5], base=3 -> [[1,2,3],[4,5]]
 *     arr=[1,2,3,4,5], base=5 -> [[1,2,3,4,5]]
 *     arr=[1,2,3,4,5], base=6 -> [[1,2,3,4,5]]
 */
function divideArray(arr, base=5){
  const dividedList = []
  let divided = []
  for(let i=0; i<arr.length; i++){
    if (i % base == 0){
      divided = []
      dividedList.push(divided);
    }
    
    let data = arr[i]
    divided.push(data);
  }
  return dividedList;
}

/*
 * 文字列からDateオブジェクトに変換する
 * 変換できない場合はnullを返す
 */
function convertToDate(msg){
  const dict = {
    "一昨日": -2,
    "昨日": -1,
    "今日": 0,
    "明日": 1,
    "明後日": 2
  }
  if(msg in dict){
    const dt = new Date();
    const interval = dict[msg];
    dt.setDate(dt.getDate() + interval);
    return dt;
  }
  const dt = new Date(msg);
  if (dt.valueOf()){
    // 日付に変換できた場合
    return dt;
  }else{
    return null;
  }
}
