function main() {
  let eventInfoList = scraping();
  pushInfoToLine(eventInfoList);
  delTrigger();
  setTrigger();
}

function setTrigger(){
  // 翌日8:00に実行するためのトリガーを作る
  const dt = new Date();
  dt.setDate(dt.getDate()+1);
  dt.setHours(8);
  dt.setMinutes(0);
  // main関数を実行する
  ScriptApp.newTrigger("main").timeBased().at(dt).create();
}

function delTrigger() {
  // setTriggerで作ったトリガーを削除する
  const triggers = ScriptApp.getProjectTriggers();
  for(const trigger of triggers){
    if(trigger.getHandlerFunction() == "main"){
      ScriptApp.deleteTrigger(trigger);
    }
  }
}
