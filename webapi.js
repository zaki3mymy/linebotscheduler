const URL_PUSH = "https://api.line.me/v2/bot/message/push";
const URL_REPLY = "https://api.line.me/v2/bot/message/reply";

function postToLineApi(url, payload){
  const headers = {
    "Content-Type": "application/json",
    "Authorization": "Bearer " + CHANNEL_ACCESS_TOKEN,
  };

  const options = {
    "method": "post",
    "headers": headers,
    "payload": JSON.stringify(payload)
  };
  const response = UrlFetchApp.fetch(url, options);
  return response;
}

function convertToLineMessages(eventInfoList){
  const messages = [];

  for(let i=0; i<eventInfoList.length; i++){
    let info = eventInfoList[i]
    messages.push({
      "type": "text",
      "text": info["title"] + "\n" + info["link"]
    });
  }
  return messages;
}

function doPost(e) {
  const event = JSON.parse(e.postData.contents).events[0];
  console.info(event);

  const replyToken = event.replyToken;
  const message = event.message.text;
  const userId = event.source.userId;

  const dt = convertToDate(message);
  if (dt == null){
    // 送られたテキストが日付を表さない場合は何もしない
    return;
  }

  const eventInfoList = scraping(dt);
  // イベント情報をLINE APIに送れるフォーマットに変換する
  const messages = convertToLineMessages(eventInfoList);
  if (messages.length == 0){
    messages.push({
      "type": "text",
      "text": "特にないみたいです"
    })
  }
  // 先頭に前文を追加する
  messages.unshift({
    "type": "text",
    "text": `${dt.getFullYear()}年${dt.getMonth()+1}月${dt.getDate()}日の予定は……`
  })
  // LINEのプッシュメッセージは1度に最大5件までなので5個ずつ送るために分割する
  const messagesList = divideArray(messages, 5);
  for (let i=0; i<messagesList.length; i++){
    const messages = messagesList[i];
    // できるだけ無料かつ無制限なreplyで返す
    if (i==0) {
      const payload = {
        "replyToken": replyToken,
        "messages": messages
      }
      postToLineApi(URL_REPLY, payload);
    } else {
      const payload = {
        "to": userId,
        "messages": messages
      }
      postToLineApi(URL_PUSH, payload);
    }
  }
}
