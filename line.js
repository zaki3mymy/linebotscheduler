var CHANNEL_ACCESS_TOKEN = "YOUR_CHANNEL_ACCESS_TOKEN"; 

function pushInfoToLine(eventInfoList){
  // eventInfo = {
  //   "title": title,
  //   "link": "https://..."
  // }
  const messages = convertToLineMessages(eventInfoList);
  if (messages.length == 0){
    messages.push({"type": "text", "text": "特にないみたいです"})
  }
  messages.unshift({"type": "text", "text": "今日の予定は……"})

  // LINEのプッシュメッセージは1度に最大5件までなので5個ずつ送るために分割する
  const messagesList = divideArray(messages, 5);
  for (let i=0; i<messagesList.length; i++){
    const messages = messagesList[i];
    const payload = {
      "messages": messages
    }
    postToLineApi(URL_BROADCAST, payload);
  }
}
